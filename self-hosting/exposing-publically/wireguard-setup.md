---
title: Wireguard Setup
description: one of the best VPN setups
published: true
date: 2021-05-01T16:10:09.938Z
tags: self hosting
editor: markdown
dateCreated: 2021-05-01T16:10:09.938Z
---

# Server side 
For the server side setup I recommend that people install wireguard using pivpn and not manually . 
The manual setup gives you bit more control but the pivpn install script has sane defaults for most basic setups.

So today we are going to be adhering to using pivpn for installation and setup of the server side 

## Using pivpn
Now pivpn originally is a concept that was meant for the raspberry pi but since the self hosting community has grown they have also adapted the project to fit a more wider range of servers .

> I have tested and this on both ubuntu and debian 10 machines but not on centos or any other OS . 
{.is-info}

So to use pivpn , you can do 
```
curl -L https://install.pivpn.io > install.sh
```
This will download a script called install.sh on your machine. Now you go over this script and get a bird's eye view of what is up and what exactly is this script doing before running it .
I have verified this script so I can vouch for this but you should read through it too 

> Never run random scripts you got off the web directly unless it is provided by some big project that hundreds of people are using every day. Even then I would recommend atleast getting a bird's eye view of it 
{.is-warning}

So to run it just do 

```
sh install.sh
```
Now this will ask you a bunch of questions, most times if you don't know the answer you can go with the default option and you will most probably get a working setup .

# Client Side 

## Get yourself a client config file
Now to connect to your vpn the first most important thing is to get yourself a client-config. This config basically contains your client keys to connect and a bunch of other info to properly configure your clients.
To get yourself a client config type in this command 
```
pivpn add 
```
This will ask you about the client name , this can be anything you are setting up so add that and it will generate a new config for you 
Now if you head to ~/configs you will the .conf file there with the name you typed in earlier

Now let's try to configure the three different types of clients 

## Winblows and MaxOs
These have GUI apps that you can get from [here](https://www.wireguard.com/install/)
After you get the apps , you can then import the client configs into them and you are gtg.

## Linux 
Now for setting this up on a linux machine you just need to install the ```wireguard-tools``` package from the package manager of your choice.

After that you can copy the client config into ```/etc/wireguard/wg0.conf```

Once that is done you can just start the systemd service 

```
sudo systemctl start wg-quick@wg0.service
```
and if you want to persist your VPN connection across reboots you can do this 

```
sudo systemctl enable wg-quick@wg0.service
```

For distros that don't use systemd,

```
wg-quick up wg0
```

Both these will basically add a new network interface called wg0 to your system and configure all the packets from that interface to head to the remote server. 

## Android 
Now for android you can generate a client config and once you finish doing that it shows you a qr code, if you scan that using the wireguard app , you can add your tunnel painlessly. 

> Please keep this config file secret and not share it with anyone. If anyone gets ahold of this file , they can connect to your VPN server and pretend that they are you. 
{.is-warning}


# Self hosting setup 
Now let's say that you have successfully setup wireguard on your linux server and connected it to the VPN (if you are running a windows server then I just have one question for you ,who hurt you sista ?), the next step is to find the ip of your machine on the wireguard network to do that , just execute 

```
ip a | grep wg0
```
From this listing find out your IP address.
Now you can do a standard [reverse-proxy](/self-hosting/exposing-publically/reverse-proxy) setup and the IP you just found out becomes your upstream IP and the port is the same one on which your application is running.














