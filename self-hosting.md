---
title: Self hosting
description: Do as I say , don't do as I do
published: true
date: 2021-05-01T17:27:47.424Z
tags: 
editor: markdown
dateCreated: 2021-04-27T16:56:59.816Z
---


# What is this ?
* Self hosting is the art of hosting all your SaaS services on your own since huge corporations can't really be trusted. 
* What follows in this section of the wiki are some not-so-comprehensive guides to self hosting your own SaaS at home or with some cloud provider


# The point of self hosting 
* A web server just has one purpose to serve static content . It's not really meant to process stuff for you, but with the advent of the web we have also seen the advancement of something called Software as a Service or SaaS.
* When you consume a SaaS , the server typically asks for some data , processes it for you and then gives you back. Some examples of SaaS include facebook,instagram,github,gitlab,basically all things hosted by google etc.
* The problem with proprietary SaaS is that you have no idea what is running on the server and what it is doing to your data, atleast if the SaaS is [Free software](http://www.gnu.org/philosophy/free-sw.html), you have some idea what the server is doing to your data so you can have some re-assurance 
* The best way hence in my opinion is to self host your SaaS as much as you can. Maybe if you have enough funds host some Services for your friends too. :) 

## A short list of SaaS providers I trust 
* nixnet.services
* fsci.in 
* disroot.org
* autistici.org
* jit.si
* Friends I personally know 
* Myself 

> I am a just a student of life documenting what I learn . I am not responsible for south park not releasing season 26 or your server burning down. Please copy stuff at your own risk
{.is-warning}
