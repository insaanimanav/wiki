---
title: Commands used in MQTT
description: 
published: true
date: 2021-05-22T15:09:03.678Z
tags: mqtt, iot
editor: markdown
dateCreated: 2021-05-22T15:08:25.300Z
---

## 1. CONNECT - Client requests a connection to the server 
The TCP/IP socket connection from the client to the server , a protocol level session must also be created using the CONNECT flow 

### Fixed Header 
|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|0    |0     |0     |1   |x       |x    |x  |x     |

the DUP , QoS and RETAIN Flags aren't used in the CONNECT Message. 

### Variable Header 
This contains  the Following Details

* Protocol Name 
* Protocol Version Number 
* User name Flag (1)
* Password Flag(1)
* Clean Session Flag (1)
* Keep Alive Timer 
* Will Message 
     * Will flag is set (1)
     * Will QoS field is 1
     * Will RETAIN flag  
     
### Payload 
* Client Identifier 
* Topic 
* Will Message 
* Username 
* Password 

### Response 
* The response to a CONNECT Message is CONNACK.
* If the broker doesn't receive a CONNECT Message in a reasonable amount of time after the TCP/IP connection has been established, it should close the connection 
* If the client doesn't receieve a CONNACK message in a reasonable amount of time after sending the CONNECT message, it can close the socket and create a new one 
* If an INVALID CONNECT message has been sent, the server must close the connection

## 2. PUBLISH

* A PUBLISH message is sent by the client to the server for distribution to interested subscribers.
* Each PUBLISH Message is associated with a topic name . 
* A topic is a hierarchical name space that defines a taxonomy of information sources for which subscribers can register an interest.
* if  client subscribes to one or more  topics , any message published to those topics gets sent to these clients as well.

### Fixed Header 
|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|0    |0     |1     |1   |0       |0    |1  |0     |

### Variable Header 

* #### Topic Name 
    * A UTF Encoded string 
    * Should not contain wildcard characters 
    * Message ID 
* #### Message ID
    * Is present for all QoS >= 1
 
* #### Payload 
    *  Contains the data to be published 
    *  Generally it is OK for a Message to contain an empty Payload as well 

* #### Response 
    * The response depends on the QoS Level . The below table shows the expected responses 

| QoS Level | Response |
|-----------|----------|
| 0         | None     |
| 1         | PUBACK   |
| 2         | PUBREC   |
 
* #### Action 
    * If QoS is 0 - Make the message available to any interested parties
    * If QoS is 1 - Log the message to persistent storage , make it available to all the subscribers. Send the PUBACK message 
    * If QoS is 2 - Log the message , do not make it available to the interested parties yet and sent a PUBACK message to the sender.

## 3. SUBSCRIBE - Subscribe to named topics 
* The SUBSCRIBE message allows a client to register it's interest in a topic being advertised by the server 
* Messages published to these topics are  delivered from the server to the client as PUBLISH message.

### Fixed header 

|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|1    |0     |0     |0   |0       |0    |1  |x     |

### Variable header 
* #### Message ID
    * Since QoS > 1 it contains a message ID

### Payload 
* Contains a list of topics the client want to subscribe to and the respective QoS Levels the client wants to subscribe to. 
* The strings are UTF encoded 

### Short note 
* Assuming that the required QoS Level is granted, the client then receives the messages on less than or equal to that QoS Level.

* Let's say that a client has QoS level 1 to a message and that message is being published at QoS level 0 , So the server will publish the message at QoS level 0. If some other message is being sent at QoS level 2 then it will be downgraded to QoS level 1. 
* This is same as saying that if I am subscribing to QoS 2 I would like to receive the messages as they are published on the same QoS Level.


## 4. UNSUBSCRIBE - Unsubscribe from named topics 
An unsubscribe message is sent by the client to the server to unsubscribe from named topics 

### Fixed header 

|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|1    |0     |1     |0   |0       |0    |1  |x     |

### Variable header 

* #### Message ID 

### Payload 
* #### List of topics to Unsubscribe
    * The client unsubscribes from the list of topics named in the payload 
    * The strings are UTF encoded


## 5. PINGREQ - Ping request 
This message is used to ask the Broker "Are you Alive ?"

### Fixed Header 
|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|1    |0     |1     |1   |x       |x    |x  |x     |

### Variable Header 
The Variable Header contains the Message ID 

### Payload 
No payload 

###  Response 
The Response to this is a PINGRESP Message 

## 6. PINGRESP 
A Pingresp message  is the response sent by the server to a PINGRESQ Message saying that "Yes I am Alive"

### Fixed Header
|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|1    |1     |0     |1   |x       |x    |x  |x     |

### Variable Header 
No Variable Header

### Payload 
No payload 

## 7.DISCONNECT Message 
* The DISCONNECT Message is sent from the client to the server to indicate that it is about to close its TCP/IP connection. 
* If the client had connected with a clean session flag all the information about the client will be removed. 
* A server should not rely on the client to close the TCP/IP Session after receiving the disconnect message. 

### Fixed Header
|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|
|1    |1     |1     |0   |x       |x    |x  |x     |

### Variable Header 
No Varible Header 

### Payload 
No Payload 

## Note
There are a bunch of Acknowledge messages that have not been covered here , you can read about them 








