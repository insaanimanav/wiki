---
title: MQTT Headers
description: about the headers used in mqtt
published: true
date: 2021-05-22T15:06:14.743Z
tags: mqtt, iot
editor: markdown
dateCreated: 2021-05-22T15:06:14.743Z
---

# The MQTT Protocol 

## The basic Idea 
* MQTT(MQ Telemetry transport) is a protocol which is based on the pub-sub model. 
* The pub-sub model means that there is one publisher and multiple subscribers  * You always talk in terms of topics 
* Clients that want a certain piece of information subscribe to a particular topic and the server/broker gives them that piece of information.
* A small transport overhead
* Payload agnostic protocol 

## Format of the Message Header

* Each MQTT message has a fixed header and a variable header 
* The fixed header is 2 bytes long. 

### The first byte 
|  7  | 6    |  5   | 4  | 3      | 2   | 1 | 0    |
| ----| ---- | ---- |----|--------|-----|---|------|
|type |type  |type  |type|DUP Flag|QoS  |QoS|RETAIN|

#### Message type 
Message type is the first 4 bits of  each MQTT header , they can be of various types which have been discussed later . Some example codes for message type are given below 
CONNECT  - 01
CONNACK - 02

#### DUP Flag (Duplicate Delivery)
* This flag is set to 1 if a  message is being sent again 
* This applies to messages where QoS is greater than 1
* The variable header here also consists of the Message ID 

#### QoS Flag 
* This flag is basically responsible for delivery of a PUBLISH message.
* The following is a representation of the QoS Levels 

| Value | bit 2  | bit 1  | Description   |                  |
|-------|--------|--------|---------------|------------------|
| 0     | 0      | 0      | At most once  | Fire and Forget  |
| 1     | 0      | 1      | At least once | Ack Delivery     |
| 2     | 0      | 2      | Exactly once  | Assured Delivery |


#### RETAIN Flag
* Used only in PUBLISH Messages 
* When a Client sends a PUBLISH message and this flag is set , It means that  the Broker should retain that message even after publishing to all the subscribers 
* This is useful when some publishers send the message on a "Report By Exception Basis" Where there might be sometime between two messages 
* When a new client subscribes to a topic , the Broker should publish the retained messages to that client 
* Retained messages should be kept over restarts of the Broker 

### The second byte 
The second byte of an MQTT Header basically contains the variable header and the payload data 

#### Variable Header 
The Variable Header contains the following things 

* ##### Protocol Name (Used in the CONNECT Message)
    * The protocol name is a UTF-8 encoded string that represents the name MQIsdp , Capitalized as shown.

* ##### Protocol Version (Used in the CONNECT Message)
    * This is an 8 bit unsigned value that represents the revision Level of the protocol 

* ##### Clean Session Flag 
    * If set to 0 then the broker must store all the messages sent by the client even after the client disconnects. The server must also maintain the state of the inflight messages so that they can be redelivered when the client reconnects
    * If set to 1 then the broker should discard all previous information about the client and treat the connection as clean 

* ##### Will Message 
    * The variable header also contains a will message 
    * The will message contains several flags. Here is a [more detailed guide](http://www.steves-internet-guide.com/mqtt-last-will-example/)

* ##### Username Password 
    * Both these are also used in the Variable Header of the CONNECT flag for authentication of clients wherever needed

* ##### Keep Alive Timer 
    * The Keep alive timer is antoher field in the the variable header 
    * It defines in seconds the the max time interval in between messages received or sent 
    * The client has a responsibility of sending messages within the  keep alive timer period
    * If the broker doesn't receive the message within the period it can close the connection without waiting for the long TCP / IP timeout
* ##### Connect return code 
    * This is sent in the CONNACK message to describe the status of the CONNECT message 
    * A return code of 0 generally means an established connection 

* ##### Topic Name
    * The topic name is present in the variable header of the PUBLISH message 
    * This is a UTF-8 encoded string 

* ##### Message ID 
    * This is only present if the QoS Levels are 1 or 2
    * This is a 16bit unsigned int that must be unique amongst the set on inflight messages 
    * A client will usually maintain its own list of message IDs seperate from the server IDs


